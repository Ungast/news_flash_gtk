use super::error::{ContentPageError, ContentPageErrorKind};
use super::header_selection::HeaderSelection;
use crate::account_popover::AccountPopover;
use crate::app::Action;
use crate::i18n::i18n;
use crate::main_window_state::MainWindowState;
use crate::tag_popover::TagPopover;
use crate::util::{BuilderHelper, GtkUtil, Util};
use failure::ResultExt;
use gio::{ActionMapExt, Menu, SimpleAction};
use glib::{clone, object::Cast, translate::ToGlib, Continue, Sender};
use gtk::{
    Button, ButtonExt, EntryExt, Image, ImageExt, MenuButton, MenuButtonExt, Popover, PopoverExt, SearchEntry,
    SearchEntryExt, SettingsExt, Stack, StackExt, ToggleButton, ToggleButtonExt, Widget, WidgetExt,
};
use libhandy::{SearchBar, SearchBarExt};
use log::warn;
use news_flash::models::{FatArticle, Marked, PluginCapabilities, PluginID, PluginIcon, Read};
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::sync::Arc;

pub struct ContentHeader {
    sender: Sender<Action>,
    state: Arc<RwLock<MainWindowState>>,
    account_button_image: Image,
    account_popover: AccountPopover,
    view_switcher_stack: Stack,
    update_stack: Stack,
    update_button: Button,
    offline_button: Button,
    offline_popover: Popover,
    online_popover: Popover,
    search_button: ToggleButton,
    search_entry: SearchEntry,
    mark_all_read_button: Button,
    mark_all_read_stack: Stack,
    scrap_content_button: ToggleButton,
    scrap_content_stack: Stack,
    scrap_content_event: RwLock<Option<usize>>,
    tag_button: MenuButton,
    tag_popover: RwLock<Option<TagPopover>>,
    more_actions_button: MenuButton,
    more_actions_stack: Stack,
    mark_article_button: ToggleButton,
    mark_article_read_button: ToggleButton,
    mark_article_stack: Stack,
    mark_article_read_stack: Stack,
    mark_article_event: RwLock<Option<usize>>,
    mark_article_read_event: RwLock<Option<usize>>,
}

impl ContentHeader {
    pub fn new(
        builder: &BuilderHelper,
        state: &Arc<RwLock<MainWindowState>>,
        sender: Sender<Action>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) -> Self {
        let account_button = builder.get::<MenuButton>("account_button");
        let account_button_image = builder.get::<Image>("account_button_image");
        let view_switcher_stack = builder.get::<Stack>("empty_placeholder_stack");
        let update_button = builder.get::<Button>("update_button");
        let update_stack = builder.get::<Stack>("update_stack");
        let offline_button = builder.get::<Button>("offline_status_button");
        let offline_popover = builder.get::<Popover>("offline_popover");
        let online_popover = builder.get::<Popover>("online_popover");
        let menu_button = builder.get::<MenuButton>("menu_button");
        let tag_button = builder.get::<MenuButton>("tag_button");
        let more_actions_button = builder.get::<MenuButton>("more_actions_button");
        let more_actions_stack = builder.get::<Stack>("more_actions_stack");
        let search_button = builder.get::<ToggleButton>("search_button");
        let search_bar = builder.get::<SearchBar>("search_bar");
        let search_entry = builder.get::<SearchEntry>("search_entry");
        let mark_all_read_button = builder.get::<Button>("mark_all_button");
        let mark_all_read_stack = builder.get::<Stack>("mark_all_stack");
        let mark_article_button = builder.get::<ToggleButton>("mark_article_button");
        let mark_article_read_button = builder.get::<ToggleButton>("mark_article_read_button");
        let mark_article_stack = builder.get::<Stack>("mark_article_stack");
        let mark_article_read_stack = builder.get::<Stack>("mark_article_read_stack");
        let scrap_content_button = builder.get::<ToggleButton>("scrap_content_button");
        let scrap_content_stack = builder.get::<Stack>("scrap_article_stack");

        let account_popover = AccountPopover::new();
        account_button.set_popover(Some(&account_popover.widget));

        mark_all_read_button.connect_clicked(clone!(
            @weak mark_all_read_stack,
            @strong sender => @default-panic, move |button|
        {
            button.set_sensitive(false);
            mark_all_read_stack.set_visible_child_name("spinner");
            Util::send(&sender, Action::SetSidebarRead);
        }));

        offline_button.connect_clicked(clone!(@strong sender => @default-panic, move |_button| {
            Util::send(&sender, Action::SetOfflineMode(false));
        }));

        let view_switcher_timeout: Arc<RwLock<Option<u32>>> = Arc::new(RwLock::new(None));
        let header_selection = Arc::new(RwLock::new(HeaderSelection::All));

        let tag_popover: RwLock<Option<TagPopover>> = RwLock::new(None);

        Self::setup_view_switcher(&view_switcher_stack, &header_selection, &view_switcher_timeout, &sender);
        Self::setup_update_button(&update_button, &sender);
        Self::setup_search_button(&search_button, &search_bar);
        Self::setup_search_bar(&search_bar, &search_button, &search_entry);
        Self::setup_search_entry(&search_entry, &sender);

        Self::setup_menu_button(&menu_button, &sender, features);
        Self::setup_more_actions_button(&more_actions_button, &sender);

        let header = ContentHeader {
            sender,
            state: state.clone(),
            account_popover,
            account_button_image,
            view_switcher_stack,
            update_stack,
            update_button,
            offline_button,
            offline_popover,
            online_popover,
            search_button,
            search_entry,
            mark_all_read_button,
            mark_all_read_stack,
            scrap_content_button,
            scrap_content_stack,
            scrap_content_event: RwLock::new(None),
            tag_button,
            tag_popover,
            more_actions_button,
            more_actions_stack,
            mark_article_button,
            mark_article_read_button,
            mark_article_stack,
            mark_article_read_stack,
            mark_article_event: RwLock::new(None),
            mark_article_read_event: RwLock::new(None),
        };

        header.show_article(None, &Arc::new(RwLock::new(None)), features);
        header
    }

    pub fn set_account(&self, id: Option<&PluginID>, user_name: Option<&str>) -> Result<(), ContentPageError> {
        let scale = GtkUtil::get_scale(&self.account_button_image);
        let mut icon = GtkUtil::create_surface_from_icon_name("feed-service-generic", 16, scale);
        let mut vector_data = None;
        let mut user = i18n("Unitialized");

        if let Some(id) = id {
            let list = NewsFlash::list_backends();
            if let Some(info) = list.get(id) {
                user = match user_name {
                    Some(user_name) => user_name.into(),
                    None => info.name.clone(),
                };
                if let Some(plugin_icon) = &info.icon_symbolic {
                    match plugin_icon {
                        PluginIcon::Vector(vector_icon) => {
                            let pref_dark = if let Some(gtk_settings) = gtk::Settings::get_default() {
                                gtk_settings.get_property_gtk_application_prefer_dark_theme()
                            } else {
                                false
                            };
                            let color = if pref_dark { "ffffff" } else { "000000" };

                            let colored_data = Util::symbolic_icon_set_color(&vector_icon.data, color)
                                .context(ContentPageErrorKind::MetaData)?;
                            icon = GtkUtil::create_surface_from_bytes(&colored_data, 16, 16, scale)
                                .context(ContentPageErrorKind::MetaData)?;
                            let mut vector_icon = vector_icon.clone();
                            vector_icon.data = colored_data;
                            vector_data = Some(vector_icon);
                        }
                        PluginIcon::Pixel(_icon) => {
                            warn!("Pixel based icon not valid for account button");
                        }
                    }
                }
            }
        }

        self.account_button_image.set_from_surface(Some(&icon));
        self.account_popover.set_account(vector_data, &user);

        Ok(())
    }

    pub fn start_sync(&self) {
        self.update_button.set_sensitive(false);
        self.update_stack.set_visible_child_name("spinner");
        self.update_stack.show_all();
    }

    pub fn finish_sync(&self) {
        self.update_button.set_sensitive(true);
        self.update_stack.set_visible_child_name("icon");
        self.update_stack.show_all();
    }

    pub fn is_search_focused(&self) -> bool {
        self.search_button.get_active() && self.search_entry.has_focus()
    }

    pub fn focus_search(&self) {
        // shortcuts ignored when focues -> no need to hide seach bar on keybind (ESC still works)
        self.search_button.set_active(true);
        self.search_entry.grab_focus();
    }

    pub fn set_view_switcher_stack(&self, child_name: &str) {
        self.view_switcher_stack
            .set_visible_child_name(&format!("{}_placeholder", child_name));
    }

    fn setup_view_switcher(
        view_switcher_stack: &Stack,
        header_selection: &Arc<RwLock<HeaderSelection>>,
        view_switcher_timeout: &Arc<RwLock<Option<u32>>>,
        sender: &Sender<Action>,
    ) {
        view_switcher_stack.connect_property_visible_child_name_notify(clone!(
            @strong header_selection,
            @strong view_switcher_timeout,
            @strong sender => @default-panic, move |stack|
        {
            if let Some(child_name) = stack.get_visible_child_name() {
                let child_name = child_name.to_string();

                *header_selection.write() = if child_name.starts_with("unread") {
                    HeaderSelection::Unread
                } else if child_name.starts_with("marked") {
                    HeaderSelection::Marked
                } else {
                    HeaderSelection::All
                };

                if view_switcher_timeout.read().is_some() {
                    return;
                }

                Self::stack_switched(&sender, &header_selection, &view_switcher_timeout);
            }
        }));
    }

    fn stack_switched(
        sender: &Sender<Action>,
        header_selection: &Arc<RwLock<HeaderSelection>>,
        view_switcher_timeout: &Arc<RwLock<Option<u32>>>,
    ) {
        Util::send(sender, Action::HeaderSelection((*header_selection.read()).clone()));

        if view_switcher_timeout.read().is_some() {
            return;
        }

        let mode_before_cooldown = (*header_selection.read()).clone();
        view_switcher_timeout.write().replace(
            glib::timeout_add_local(
                250,
                clone!(
                    @strong header_selection,
                    @strong view_switcher_timeout,
                    @strong sender => @default-panic, move ||
                {
                    view_switcher_timeout.write().take();
                    if mode_before_cooldown != *header_selection.read() {
                        Self::stack_switched(
                            &sender,
                            &header_selection,
                            &view_switcher_timeout,
                        );
                    }
                    Continue(false)
                }),
            )
            .to_glib(),
        );
    }

    fn setup_update_button(button: &Button, sender: &Sender<Action>) {
        button.connect_clicked(clone!(@strong sender => @default-panic, move |_button| {
            Util::send(&sender, Action::Sync);
        }));
    }

    fn setup_search_button(search_button: &ToggleButton, search_bar: &SearchBar) {
        search_button.connect_toggled(clone!(@weak search_bar => @default-panic, move |button| {
            if button.get_active() {
                search_bar.set_search_mode(true);
            } else {
                search_bar.set_search_mode(false);
            }
        }));
    }

    fn setup_search_bar(search_bar: &SearchBar, search_button: &ToggleButton, search_entry: &SearchEntry) {
        search_bar.connect_entry(search_entry);
        search_bar.connect_property_search_mode_enabled_notify(
            clone!(@weak search_button => @default-panic, move |search_bar| {
                if !search_bar.get_search_mode() {
                    search_button.set_active(false);
                }
            }),
        );
    }

    fn setup_search_entry(search_entry: &SearchEntry, sender: &Sender<Action>) {
        search_entry.connect_search_changed(clone!(@strong sender => @default-panic, move |search_entry| {
            Util::send(&sender, Action::SearchTerm(search_entry.get_text().as_str().into()));
        }));
    }

    fn setup_menu_button(
        button: &MenuButton,
        sender: &Sender<Action>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) {
        let show_shortcut_window_action = SimpleAction::new("shortcut-window", None);
        show_shortcut_window_action.connect_activate(
            clone!(@strong sender => @default-panic, move |_action, _parameter| {
                Util::send(&sender, Action::ShowShortcutWindow);
            }),
        );

        let show_about_window_action = SimpleAction::new("about-window", None);
        show_about_window_action.connect_activate(
            clone!(@strong sender => @default-panic, move |_action, _parameter| {
                Util::send(&sender, Action::ShowAboutWindow);
            }),
        );

        let settings_window_action = SimpleAction::new("settings", None);
        settings_window_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ShowSettingsWindow);
        }));

        let discover_dialog_action = SimpleAction::new("discover", None);
        discover_dialog_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ShowDiscoverDialog);
        }));

        let quit_action = SimpleAction::new("quit-application", None);
        quit_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::QueueQuit);
        }));

        let import_opml_action = SimpleAction::new("import-opml", None);
        import_opml_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ImportOpml);
        }));

        let export_opml_action = SimpleAction::new("export-opml", None);
        export_opml_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ExportOpml);
        }));

        // account popover
        let reset_account_action = SimpleAction::new("reset-account", None);
        reset_account_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ShowResetPage);
        }));

        let retry_login_action = SimpleAction::new("retry-login", None);
        retry_login_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            log::info!("retry login");
            Util::send(&sender, Action::RetryLogin);
        }));

        let offline_action = SimpleAction::new("go-offline", None);
        offline_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            log::info!("going offline");
            Util::send(&sender, Action::SetOfflineMode(true));
        }));

        if let Ok(main_window) = GtkUtil::get_main_window(button) {
            main_window.add_action(&show_shortcut_window_action);
            main_window.add_action(&show_about_window_action);
            main_window.add_action(&settings_window_action);
            main_window.add_action(&discover_dialog_action);
            main_window.add_action(&quit_action);
            main_window.add_action(&import_opml_action);
            main_window.add_action(&export_opml_action);
            main_window.add_action(&reset_account_action);
            main_window.add_action(&retry_login_action);
            main_window.add_action(&offline_action);
        }

        if let Some(features) = features.read().as_ref() {
            discover_dialog_action.set_enabled(features.contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        }

        let about_model = Menu::new();
        about_model.append(Some(&i18n("Shortcuts")), Some("win.shortcut-window"));
        about_model.append(Some(&i18n("About")), Some("win.about-window"));
        about_model.append(Some(&i18n("Quit")), Some("win.quit-application"));

        let im_export_model = Menu::new();
        im_export_model.append(Some(&i18n("Import OPML")), Some("win.import-opml"));
        im_export_model.append(Some(&i18n("Export OPML")), Some("win.export-opml"));

        let main_model = Menu::new();
        main_model.append(Some(&i18n("Settings")), Some("win.settings"));
        main_model.append(Some(&i18n("Discover Feeds")), Some("win.discover"));
        main_model.append_section(Some(""), &im_export_model);
        main_model.append_section(Some(""), &about_model);

        button.set_menu_model(Some(&main_model));
    }

    fn setup_more_actions_button(button: &MenuButton, sender: &Sender<Action>) {
        let close_article_action = SimpleAction::new("close-article", None);
        close_article_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::CloseArticle);
        }));

        let export_article_action = SimpleAction::new("export-article", None);
        export_article_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::ExportArticle);
        }));

        let open_article_action = SimpleAction::new("open-article-in-browser", None);
        open_article_action.connect_activate(clone!(@strong sender => @default-panic, move |_action, _parameter| {
            Util::send(&sender, Action::OpenSelectedArticle);
        }));

        if let Ok(main_window) = GtkUtil::get_main_window(button) {
            main_window.add_action(&close_article_action);
            main_window.add_action(&open_article_action);
            main_window.add_action(&export_article_action);
        }

        let model = Menu::new();
        model.append(Some(&i18n("Export Article")), Some("win.export-article"));
        model.append(Some(&i18n("Open in browser")), Some("win.open-article-in-browser"));
        model.append(Some(&i18n("Close Article")), Some("win.close-article"));
        button.set_menu_model(Some(&model));
        button.set_sensitive(false);
    }

    fn unread_button_state(article: Option<&FatArticle>) -> (&str, bool) {
        match article {
            Some(article) => match article.unread {
                Read::Read => ("read", false),
                Read::Unread => ("unread", true),
            },
            None => ("read", false),
        }
    }

    fn marked_button_state(article: Option<&FatArticle>) -> (&str, bool) {
        match article {
            Some(article) => match article.marked {
                Marked::Marked => ("marked", true),
                Marked::Unmarked => ("unmarked", false),
            },
            None => ("unmarked", false),
        }
    }

    pub fn show_article(
        &self,
        article: Option<&FatArticle>,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) {
        let sensitive = article.is_some();

        let (unread_icon, unread_active) = Self::unread_button_state(article);
        let (marked_icon, marked_active) = Self::marked_button_state(article);

        self.mark_article_stack.set_visible_child_name(marked_icon);
        self.mark_article_read_stack.set_visible_child_name(unread_icon);

        GtkUtil::disconnect_signal(*self.mark_article_read_event.read(), &self.mark_article_read_button);
        GtkUtil::disconnect_signal(*self.mark_article_event.read(), &self.mark_article_button);
        GtkUtil::disconnect_signal(*self.scrap_content_event.read(), &self.scrap_content_button);

        self.mark_article_button.set_active(marked_active);
        self.mark_article_read_button.set_active(unread_active);

        let show_scraped_content = if let Some(fat_article) = article {
            fat_article.scraped_content.is_some() && self.state.read().get_prefer_scraped_content()
        } else {
            false
        };
        self.scrap_content_button.set_active(show_scraped_content);

        self.mark_article_event.write().replace(
            self.mark_article_button
                .connect_toggled(clone!(
                    @weak self.mark_article_stack as toggle_stack,
                    @strong self.sender as sender => @default-panic, move |toggle_button|
                {
                    if toggle_button.get_active() {
                        toggle_stack.set_visible_child_name("marked");
                    } else {
                        toggle_stack.set_visible_child_name("unmarked");
                    }
                    Util::send(&sender, Action::ToggleArticleMarked);
                }))
                .to_glib() as usize,
        );

        self.mark_article_read_event.write().replace(
            self.mark_article_read_button
                .connect_toggled(clone!(
                    @weak self.mark_article_read_stack as toggle_stack,
                    @strong self.sender as sender => @default-panic, move |toggle_button|
                {
                    if toggle_button.get_active() {
                        toggle_stack.set_visible_child_name("unread");
                    } else {
                        toggle_stack.set_visible_child_name("read");
                    }
                    Util::send(&sender, Action::ToggleArticleRead);
                }))
                .to_glib() as usize,
        );

        self.scrap_content_event.write().replace(
            self.scrap_content_button
                .connect_clicked(clone!(
                    @strong self.sender as sender,
                    @strong self.state as state => @default-panic, move |button|
                {
                    if button.get_active() {
                        state.write().set_prefer_scraped_content(true);
                        Util::send(&sender, Action::StartGrabArticleContent);
                    } else {
                        state.write().set_prefer_scraped_content(false);
                        Util::send(&sender, Action::RedrawArticle);
                    }
                }))
                .to_glib() as usize,
        );

        self.more_actions_button.set_sensitive(sensitive);

        if !self.state.read().get_offline() {
            let mut tag_support = false;
            if let Some(features) = features.read().as_ref() {
                tag_support = features.contains(PluginCapabilities::SUPPORT_TAGS);
            }

            self.mark_article_button.set_sensitive(sensitive);
            self.mark_article_read_button.set_sensitive(sensitive);
            self.scrap_content_button.set_sensitive(sensitive);
            self.upadate_tags(&article, news_flash, tag_support);
        }
    }

    fn upadate_tags(
        &self,
        article: &Option<&FatArticle>,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
        tag_support: bool,
    ) {
        if let Some(article) = article {
            if let Some(tag_popover) = self.tag_popover.read().as_ref() {
                tag_popover.disconnect();
            }
            let popover = TagPopover::new(&article.article_id, news_flash, &self.sender);
            self.tag_button
                .set_popover(if tag_support { Some(&popover.widget) } else { None });
            self.tag_popover.write().replace(popover);
        } else {
            let popover: Option<&Widget> = None;
            self.tag_button.set_popover(popover);
        }
    }

    pub fn start_scrap_content_spinner(&self) {
        self.scrap_content_button.set_sensitive(false);
        self.scrap_content_stack.set_visible_child_name("spinner");
    }

    pub fn stop_scrap_content_spinner(&self) {
        self.scrap_content_button.set_sensitive(true);
        self.scrap_content_stack.set_visible_child_name("image");
    }

    pub fn start_more_actions_spinner(&self) {
        self.more_actions_button.set_sensitive(false);
        self.more_actions_stack.set_visible_child_name("spinner");
    }

    pub fn stop_more_actions_spinner(&self) {
        self.more_actions_button.set_sensitive(true);
        self.more_actions_stack.set_visible_child_name("image");
    }

    pub fn finish_mark_all_read(&self) {
        self.mark_all_read_button.set_sensitive(true);
        self.mark_all_read_stack.set_visible_child_name("image");
    }

    pub fn set_offline(&self, offline: bool) {
        self.offline_button.set_visible(offline);
        self.update_button.set_visible(!offline);
        self.mark_all_read_button.set_sensitive(!offline);
        self.mark_article_button.set_sensitive(!offline);
        self.mark_article_read_button.set_sensitive(!offline);
        if offline {
            self.offline_popover.popup();
            self.online_popover.popdown();
        } else {
            self.offline_popover.popdown();
            self.online_popover.popup();
        }
        if let Ok(main_window) = GtkUtil::get_main_window(&self.offline_button) {
            if let Some(import_opml_action) = main_window.lookup_action("import-opml") {
                import_opml_action
                    .downcast::<SimpleAction>()
                    .expect("downcast Action to SimpleAction")
                    .set_enabled(!offline);
            }
            if let Some(discover_action) = main_window.lookup_action("discover") {
                discover_action
                    .downcast::<SimpleAction>()
                    .expect("downcast Action to SimpleAction")
                    .set_enabled(!offline);
            }
            if let Some(offline_action) = main_window.lookup_action("go-offline") {
                offline_action
                    .downcast::<SimpleAction>()
                    .expect("downcast Action to SimpleAction")
                    .set_enabled(!offline);
            }
        }
    }
}
