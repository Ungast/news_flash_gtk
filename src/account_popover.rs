use crate::util::{BuilderHelper, GtkUtil};
use gtk::{Image, ImageExt, Label, LabelExt, Popover};
use news_flash::models::VectorIcon;

pub struct AccountPopover {
    pub widget: Popover,
    user_label: Label,
    logo: Image,
}

impl AccountPopover {
    pub fn new() -> Self {
        let builder = BuilderHelper::new("account_popover");
        let popover = builder.get::<Popover>("account_popover");
        let user_label = builder.get::<Label>("user_label");
        let logo = builder.get::<Image>("logo");

        AccountPopover {
            widget: popover,
            user_label,
            logo,
        }
    }

    pub fn set_account(&self, vector_icon: Option<VectorIcon>, user_name: &str) {
        self.user_label.set_text(user_name);

        let scale = GtkUtil::get_scale(&self.logo);
        let mut icon = GtkUtil::create_surface_from_icon_name("feed-service-generic", 64, scale);
        if let Some(vector_icon) = vector_icon {
            icon = GtkUtil::create_surface_from_bytes(&vector_icon.data, vector_icon.width, vector_icon.height, scale)
                .expect("Failed to create surface from service icon");
        }

        self.logo.set_from_surface(Some(&icon));
    }
}
